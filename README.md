# cis-c-tool

ARCHIVED!!!

请前往 https://gitee.com/zongzw/f5-tool-grep-resources 获取最新版本。

## 介绍
此工具用于抓取指定BIG-IP上的某个或者所有partition下的资源列表，方便用户对各个partition资源的概览和管理。

通过输出到stdout或者文件查看具体资源列表。

当输出目标为“bigip”时，此工具即被用作[CIS-C程序](https://cis-c.f5se.io/)的迁移工具。
迁移相关概念及步骤请参考：[迁移](https://cis-c.f5se.io/Operation-and-troubleshooting/migration/).

当`--output bigip`时，cis-c-tool程序会将收集到的bigip上的资源内容以特定格式保存到目标BIG-IP设备的Internal Data Group中。

该格式可以被CIS-C识别，并在此基础上实现配置变更或者增删。此格式可以通过指定`--output stdout`或者`--output <filepath>`输出查看。


## 工具使用方法

```
Usage of ./kic-tool-darwin:
  -bigip-password string
        BIG-IP password for connection, i.e. admin (default "admin")
  -bigip-url string
        BIG-IP url, i.e. https://enr.yuxn.com:8443
  -bigip-username string
        BIG-IP username for connection, i.e. admin (default "admin")
  -config string
        config file that contains basic config items. (default "./config.json")
  -ignore-common
        doesn't grab resources for Common (default true)
  -log-level string
        logging level: debug info warn error (default "info")
  -output string
        The output for writing to: file://./result.json bigip stdout (default "stdout")
  -overwrite
        overwrite the output if the target already exists
  -partition value
        The partitions for grabbing
  -root-kind value
        The resource kinds for grabbing
  -skip-kind value
        The resource kinds excluded for grabbing
```

另外，可参考 [`config.json.tmpl`](https://gitee.com/zongzw/kic-tool/blob/master/config.json.tmpl) 减少命令行输入参数的数量。

### 输出结果示例

```
2022/08/11 22:06:45.758216 [INFO]  Running with options: 
2022/08/11 22:06:45.758364 [INFO]          partitions : [all]
2022/08/11 22:06:45.758368 [INFO]          root-kinds : [ltm sys]
2022/08/11 22:06:45.758371 [INFO]          skip-kinds : <nil>
2022/08/11 22:06:45.759771 [INFO]           overwrite : true
2022/08/11 22:06:45.759790 [INFO]       ignore-common : true
2022/08/11 22:06:45.759794 [INFO]      bigip-password : P@ssw0rd123
2022/08/11 22:06:45.759797 [INFO]      bigip-username : admin
2022/08/11 22:06:45.759799 [INFO]              output : bigip
2022/08/11 22:06:45.759802 [INFO]           log-level : info
2022/08/11 22:06:45.759804 [INFO]           bigip-url : https://10.250.18.105:8443
2022/08/11 22:06:45.759858 [INFO]  Initializing BIG-IP: https://10.250.18.105:8443
2022/08/11 22:06:45.891904 [INFO]  Starting to grab resources: [ltm sys] for partitions: [all]
2022/08/11 22:06:45.919468 [INFO]  Partition list: [Common cis-c-6]
2022/08/11 22:06:45.919483 [INFO]  => Grabbing partition: Common
2022/08/11 22:06:45.919486 [INFO]  Ignore 'Common' partition grabbing, use --ignore-common=false to grab 'Common'
2022/08/11 22:06:45.919489 [INFO]  => Grabbing partition: cis-c-6
2022/08/11 22:06:49.125591 [INFO]   -> res : ltm/profile/client-ssl cafetls
2022/08/11 22:06:49.125611 [INFO]   -> res : ltm/profile/client-ssl cafetls-1-
2022/08/11 22:06:50.735181 [INFO]   -> res : ltm/pool coffee_pool
2022/08/11 22:06:50.968123 [INFO]   -> res : ltm/virtual https_vs
2022/08/11 22:06:50.997991 [INFO]   -> res : ltm/virtual-address 192.0.2.111
2022/08/11 22:06:52.418523 [INFO]   -> res : sys/file/ssl-cert coffeecert.crt
2022/08/11 22:06:52.418560 [INFO]   -> res : sys/file/ssl-cert teacert.crt
2022/08/11 22:06:52.485776 [INFO]   -> res : sys/file/ssl-key coffeecert.key
2022/08/11 22:06:52.485812 [INFO]   -> res : sys/file/ssl-key teacert.key
2022/08/11 22:06:55.684541 [INFO]  Writing cis-c-6 to bigip
2022/08/11 22:06:55.736109 [WARN]  data-group with name cis-c-6 already exists on bigip. ** Overwrited **.
2022/08/11 22:06:55.936359 [INFO]  Finished cis-c-6 to bigip
```
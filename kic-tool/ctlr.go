package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"regexp"
	"strings"

	f5_bigip "gitee.com/zongzw/f5-bigip-rest/bigip"
	"gitee.com/zongzw/f5-bigip-rest/utils"
)

func grabResources(kind, partition string, skipKinds []string) (map[string]interface{}, error) {
	rlt := map[string]interface{}{}
	if utils.Contains(skipKinds, kind) {
		slog.Debugf("skip: %s", kind)
		return rlt, nil
	}
	slog.Debugf("kind: %s", kind)
	queries := "?$filter=partition+eq+" + partition
	res, err := bigip.All(kind + queries)
	if err != nil {
		if strings.Contains(err.Error(), "was not found") {
			return rlt, nil
		} else {
			return rlt, err
		}
	}
	if _, f := (*res)["items"].([]interface{}); !f {
		return rlt, nil
	}
	for _, item := range (*res)["items"].([]interface{}) {
		if isLink(item) {
			ik, _ := getLink(item)
			sub, err := grabResources(ik, partition, skipKinds)
			if err != nil {
				slog.Errorf("failed to request %s: %s", ik, err.Error())
				continue
			}
			for subk, subr := range sub {
				rlt[subk] = subr
			}
		} else {
			if name, f := item.(map[string]interface{})["name"]; f {
				if p, f := item.(map[string]interface{})["partition"]; f && partition == p {
					slog.Infof(" -> res : %s %s", kind, name)
					rlt[kind+"/"+name.(string)] = item
				}
			}
		}
	}
	return rlt, nil
}

func folderize(res map[string]interface{}) map[string]interface{} {
	rlt := map[string]interface{}{}
	for resName, resBody := range res {
		if resjson, ok := resBody.(map[string]interface{}); ok {
			var folderName string
			if subpath, fieldFound := resjson["subPath"]; fieldFound {
				folderName = subpath.(string)
			} else {
				folderName = ""
			}
			if _, folderReady := rlt[folderName]; !folderReady {
				rlt[folderName] = map[string]interface{}{}
			}
			rlt[folderName].(map[string]interface{})[resName] = resBody
		}
	}
	return rlt
}

func writeTo(target string, v interface{}) {
	if target == "stdout" {
		bres, _ := json.MarshalIndent(v, "", "    ")
		slog.Infof("Result:")
		fmt.Printf("%s\n", bres)
	} else if target == "bigip" {
		if err := bigip.DeployPartition("cis-c-tenant"); err != nil {
			slog.Errorf("failed to deploy partition cis-c-tenant %s", err.Error())
			return
		}
		for pname, pobj := range v.(map[string]interface{}) {
			bres, _ := utils.MarshalNoEscaping(pobj)
			if existing, err := bigip.LoadDataGroup(pname); err != nil {
				slog.Errorf("failed to save to bigip: %s", err.Error())
			} else {
				if existing == nil {
					pc := f5_bigip.PersistedConfig{
						AS3:  "",
						Rest: string(bres),
					}
					if err := bigip.SaveDataGroup(pname, &pc); err != nil {
						slog.Errorf(err.Error())
					}
				} else if cmdflags.Overwrite {
					slog.Warnf("data-group with name %s already exists on bigip. ** Overwrited **.", pname)
					pc := f5_bigip.PersistedConfig{
						AS3:  "",
						Rest: string(bres),
					}
					if err := bigip.SaveDataGroup(pname, &pc); err != nil {
						slog.Errorf(err.Error())
					}
				} else {
					slog.Warnf("data-group with name %s already exists on bigip, use --overwrite to write it.", pname)
				}
			}
		}
	} else {
		bres, _ := json.MarshalIndent(v, "", "    ")
		filepath := strings.Replace(target, "file://", "", 1)
		folder := path.Dir(filepath)

		if _, err := os.Stat(folder); err != nil {
			slog.Errorf(err.Error())
		} else {
			if _, err := os.Stat(filepath); err != nil {
				slog.Infof("file %s was written", filepath)
				if err := ioutil.WriteFile(filepath, bres, 0644); err != nil {
					slog.Errorf(err.Error())
				}
			} else {
				if cmdflags.Overwrite {
					slog.Warnf("file %s already exists. ** Overwrited **", filepath)
					if err := ioutil.WriteFile(filepath, bres, 0644); err != nil {
						slog.Errorf(err.Error())
					}
				} else {
					slog.Warnf("file %s already exists, use --overwrite to write it.", filepath)
				}
			}
		}
	}
}

func isLink(v interface{}) bool {
	if entry, ok := v.(map[string]interface{}); ok {
		if _, f := entry["reference"]; f {
			return true
		}
	}
	return false
}

func getLink(v interface{}) (string, string) {
	if !isLink(v) {
		return "", ""
	}
	r := v.(map[string]interface{})["reference"]
	l := r.(map[string]interface{})["link"]
	k := strings.Replace(l.(string), "https://localhost/mgmt/tm/", "", -1)
	idx := strings.LastIndex(k, "?")
	subkind := k[0:idx]
	return subkind, bigip.URL + "/mgmt/tm/" + subkind
}

func checkMigratable(outContent map[string]interface{}) {
	resGroupByType := map[string][]interface{}{}
	for pname, pobj := range outContent {
		folders := pobj.(map[string]interface{})
		for fname, fobj := range folders {
			resources := fobj.(map[string]interface{})
			for rname, _ := range resources {
				tnarr := strings.Split(rname, "/")
				kind := strings.Join(tnarr[0:len(tnarr)-1], "/")
				name := tnarr[len(tnarr)-1]
				pfn := utils.Keyname(pname, fname, name)
				if _, f := resGroupByType[kind]; !f {
					resGroupByType[kind] = []interface{}{}
				}
				resGroupByType[kind] = append(resGroupByType[kind], pfn)
			}
		}
	}

	kindlist := []string{}
	for kind, rlist := range resGroupByType {
		kindlist = append(kindlist, kind)
		valid := false
		for _, ptn := range f5_bigip.ResOrder {
			if matched, err := regexp.MatchString(ptn, kind); err == nil && matched {
				valid = true
				break
			} else if err != nil {
				slog.Errorf("Failed to match %s with patten %s", kind, ptn)
				continue
			} else if !matched {
				continue
			}
		}
		if !valid {
			slog.Infof("type '%s' is unknown, cannot do later migration, related resources: %s", kind, rlist)
		}
	}

	bklist, _ := json.MarshalIndent(kindlist, "", "  ")
	slog.Infof("kindlist: %s", bklist)
}

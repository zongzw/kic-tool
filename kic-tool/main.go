package main

import (
	f5_bigip "gitee.com/zongzw/f5-bigip-rest/bigip"
	"gitee.com/zongzw/f5-bigip-rest/utils"
)

func init() {
	if err := setupFlags(); err != nil {
		panic(err)
	}
	slog = utils.SetupLog("", cmdflags.LogLevel)
	showCmdFlags()
	slog.Infof("Initializing BIG-IP: %s", cmdflags.BigipURL)
	bigip = f5_bigip.Initialize(
		cmdflags.BigipURL, cmdflags.BigipUsername, cmdflags.BigipPassword, cmdflags.LogLevel)
	excluded = []string{
		"sys/config",
		"sys/folder",
		"ltm/node",
		"ltm/snat-translation",
	}
}

func main() {

	slog.Infof("Starting to grab resources: %v for partitions: %v",
		cmdflags.RootKinds, cmdflags.Partitions)

	partitions := []string{}
	if len(cmdflags.Partitions) == 1 && cmdflags.Partitions[0] == "all" {
		ps, err := bigip.ListPartitions()
		if err != nil {
			slog.Errorf("failed and quit: %s", err.Error())
			return
		}
		partitions = append(partitions, ps...)
	} else {
		partitions = append(partitions, cmdflags.Partitions...)
	}

	skipKinds := append(excluded, cmdflags.SkipKinds...)
	slog.Infof("Partition list: %v", partitions)
	outContent := map[string]interface{}{}
	for _, partition := range partitions {
		slog.Infof("=> Grabbing partition: %s", partition)
		resItems := map[string]interface{}{}
		if (partition == "Common" || partition == "cis-c-tenant") && cmdflags.IgnoreCommon {
			slog.Infof("Ignore '%s' partition grabbing, use --ignore-common=false to grab '%s'", partition, partition)
			continue
		}
		for _, rk := range cmdflags.RootKinds {
			res, err := grabResources(rk, partition, skipKinds)
			if err != nil {
				slog.Errorf(`{"error": "%s"}\n`, err.Error())
				return
			}
			for k, v := range res {
				resItems[k] = v
			}
		}
		folderItems := folderize(resItems)
		outContent[partition] = folderItems
	}

	slog.Infof("Writing grabbed to %s", cmdflags.Output)
	writeTo(cmdflags.Output, outContent)
	checkMigratable(outContent)
}

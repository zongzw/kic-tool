package main

type CmdFlags struct {
	BigipURL      string     `json:"bigip-url"`
	BigipUsername string     `json:"bigip-username"`
	BigipPassword string     `json:"bigip-password"`
	Partitions    arrayFlags `json:"partitions"`
	RootKinds     arrayFlags `json:"root-kinds"`
	SkipKinds     arrayFlags `json:"skip-kinds"`
	Output        string     `json:"output"`
	Overwrite     bool       `json:"overwrite"`
	LogLevel      string     `json:"log-level"`
	IgnoreCommon  bool       `json:"ignore-common"`
}

type arrayFlags []string

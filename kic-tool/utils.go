package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"gitee.com/zongzw/f5-bigip-rest/utils"
)

func (i *arrayFlags) String() string {
	return fmt.Sprint(*i)
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

func setupFlags() error {
	var config string
	flag.StringVar(&config, "config", "./config.json", "config file that contains basic config items.")

	flag.StringVar(&cmdflags.BigipURL, "bigip-url", "", "BIG-IP url, i.e. https://enr.yuxn.com:8443")
	flag.StringVar(&cmdflags.BigipPassword, "bigip-password", "admin", "BIG-IP password for connection, i.e. admin")
	flag.StringVar(&cmdflags.BigipUsername, "bigip-username", "admin", "BIG-IP username for connection, i.e. admin")
	flag.Var(&cmdflags.Partitions, "partition", "The partitions for grabbing")
	flag.Var(&cmdflags.RootKinds, "root-kind", "The resource kinds for grabbing")
	flag.StringVar(&cmdflags.Output, "output", "stdout", "The output for writing to: file://./result.json bigip stdout")
	flag.Var(&cmdflags.SkipKinds, "skip-kind", "The resource kinds excluded for grabbing")
	flag.StringVar(&cmdflags.LogLevel, "log-level", "info", "logging level: debug info warn error")
	flag.BoolVar(&cmdflags.Overwrite, "overwrite", false, "overwrite the output if the target already exists")
	flag.BoolVar(&cmdflags.IgnoreCommon, "ignore-common", true, "doesn't grab resources for Common")

	flag.Parse()

	if _, err := os.Stat(config); err == nil {
		var defconf CmdFlags
		bconf, err := ioutil.ReadFile(config)
		if err != nil {
			return err
		}
		if err := json.Unmarshal(bconf, &defconf); err != nil {
			return err
		}

		if !utils.Contains(os.Args, "--bigip-url") {
			cmdflags.BigipURL = defconf.BigipURL
		}
		if !utils.Contains(os.Args, "--bigip-password") && defconf.BigipPassword != "" {
			cmdflags.BigipPassword = defconf.BigipPassword
		}
		if !utils.Contains(os.Args, "--bigip-username") && defconf.BigipUsername != "" {
			cmdflags.BigipUsername = defconf.BigipUsername
		}

		if !utils.Contains(os.Args, "--partition") && len(defconf.Partitions) != 0 {
			cmdflags.Partitions = defconf.Partitions
		}
		if !utils.Contains(os.Args, "--root-kind") && len(defconf.RootKinds) != 0 {
			cmdflags.RootKinds = defconf.RootKinds
		}
		if !utils.Contains(os.Args, "--skip-kind") && len(defconf.SkipKinds) != 0 {
			cmdflags.SkipKinds = defconf.SkipKinds
		}
		if !utils.Contains(os.Args, "--output") && defconf.Output != "" {
			cmdflags.Output = defconf.Output
		}
		if !utils.Contains(os.Args, "--log-level") && defconf.LogLevel != "" {
			cmdflags.LogLevel = defconf.LogLevel
		}
		if !utils.Contains(os.Args, "--ignore-common") {
			var defopts map[string]interface{}
			json.Unmarshal(bconf, &defopts)
			if v, f := defopts["ignore-common"]; f {
				cmdflags.IgnoreCommon, _ = v.(bool)
			}
		}
	}

	var opts map[string]interface{}
	bopts, _ := json.Marshal(&cmdflags)
	json.Unmarshal(bopts, &opts)
	for _, r := range []string{"bigip-url"} {
		if opts[r] == "" {
			return fmt.Errorf("%s cannot be empty", r)
		}
	}
	if len(cmdflags.RootKinds) == 0 {
		cmdflags.RootKinds = []string{"ltm", "sys"}
	}

	if len(cmdflags.Partitions) == 0 {
		cmdflags.Partitions = append(cmdflags.Partitions, "all")
	}

	return nil
}

func showCmdFlags() {
	bopts, _ := json.MarshalIndent(cmdflags, "", "    ")
	var optsOut map[string]interface{}
	json.Unmarshal(bopts, &optsOut)
	slog.Infof("Running with options: ")
	for k, v := range optsOut {
		slog.Infof("   %15s : %v", k, v)
	}
}

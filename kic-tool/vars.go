package main

import (
	f5_bigip "gitee.com/zongzw/f5-bigip-rest/bigip"
	"gitee.com/zongzw/f5-bigip-rest/utils"
)

var (
	bigip    *f5_bigip.BIGIP
	cmdflags CmdFlags
	slog     utils.SLOG
	excluded []string
)
